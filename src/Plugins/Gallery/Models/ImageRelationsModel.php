<?php
namespace Creativehandles\ChGallery\Plugins\Gallery\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 28/8/19
 * Time: 5:36 PM
 */

class ImageRelationsModel extends Model
{
    protected $table = 'image_relation';
    protected $guarded=[];

}